# Verification Bot

This Delta Chat bot is useful if your device is not always online.
It allows you to generate and send out verification QR codes.

You can send a group join QR code (only as text for now, so "OPENPGP4FPR:....")
to the bot,
to make it listen to join requests
even if your main device is offline.

## Install

First, you need to set it up on a linux machine
that's always running and online (e.g. a server).

For this, go to the Delta Chat settings
and export a backup.
The bot will need it to access your account
and be able to verify contacts.
Copy it to the server with scp or rsync or so.

Then login to the server via SSH
and install the bot from this repository:

```
git clone https://git.0x90.space/missytake/verificationbot
cd verificationbot
python3 -m venv env
. env/bin/activate
pip install -e .
```

Then you need to initialize the bot
with the backup file you copied to the server:

```
verificationbot init --from-backup ../delta-chat-backup-*.tar
```

This will create the "verification bot control group",
with only you as a member.
If that doesn't happen,
it didn't work.

If the group was created,
you can run the bot:

```
verificationbot run
```

## Usage

To make the bot listen for a group join QR code,
copy-paste the text of the QR code
and send it to the "verification bot control group".

The bot will reply whether it successfully activated the QR code.

